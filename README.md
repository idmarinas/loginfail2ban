# LoginFail2Ban v1.1.0 #

*LoginFail2Ban* comprueba los intentos de conexión desde una misma IP. Si la IP lleva 5 (defecto) intentos de conexión fallidos bloquea la IP durante un tiempo para que no pueda continuar intentando la conexión a la aplicación.

Ahora se utiliza como módulo para Zend Framework 3

## Requisitos ##

* PHP >=5.6
* Composer
* Base de datos: Mysql o Sqlite

## Instalación y configuración ##

Ver la [Wiki](https://bitbucket.org/idmarinas/loginfail2ban/wiki "Wiki") para instalar y configurar *LoginFail2Ban*

