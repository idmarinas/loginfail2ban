/**
 * Para crear la tabla en una base de datos MYSQL
 */
CREATE TABLE `accounts_loginfail2ban` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ipAddress` varbinary(25) NOT NULL DEFAULT '',
  `count` tinyint(3) unsigned NOT NULL,
  `date` int(10) unsigned NOT NULL,
  `banEnd` int(10) unsigned NOT NULL,
  `banCount` tinyint(3) unsigned NOT NULL,
  `userAgent` varchar(350) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `ipAddress` (`ipAddress`),
  KEY `date` (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;