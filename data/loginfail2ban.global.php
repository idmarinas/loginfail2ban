<?php

/**
 * Todos los valores son necesarios
 */
return [
	'idmarinas_loginfail2ban' => [
		/**
		 * Nombre del evento que se activa ante un error en la autentificación
		 *
		 * Expects: string
		 * Default: authentication.error
		 */
		'event_authentication_error_name' => 'authentication.error',

		/**
		 * Nombre identificador para el evento
		 *
		 * Expects: string
		 * Default: Account
		 */
		'event_namespace_name' => 'Account',

		/**
         * Nombre que tiene el adaptador para poder ser usado por Idmarinas\LoginFail2Ban
         *
         * Expects: string
         * Default: Db\Adapter\Main
         */
		'adapter_class_name' => 'Db\Adapter\Main',

		 /**
          * Nombre de la tabla donde se guardan los datos de Idmarinas\LoginFail2Ban
          *
          * Expects: string
          * Default: accounts_loginfail2ban
          */
		'table_name' => 'accounts_loginfail2ban',

		/**
		 * Tiempo que se va a bloquear la IP si se supera el número de veces permitidas
		 *
		 * Expects int >= 300
		 * Default: 900
		 */
		'block_duration' => 900,

		/**
		 * Número de intentos que se permiten antes de bloquear la IP
		 *
		 * Expects int >= 1
		 * Default: 5
		 */
		'max_count' => 5
	]
];