<?php

namespace Idmarinas\LoginFail2Ban\Factory;

use Idmarinas\LoginFail2Ban\Options as Opciones;

use Interop\Container\ContainerInterface;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class Options implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Config');
		$config = isset($config['idmarinas_loginfail2ban']) ? $config['idmarinas_loginfail2ban'] : null;

		return new Opciones($config);
    }

    public function createService(ServiceLocatorInterface $services, $canonicalName = null, $requestedName = null)
    {
        return $this($services, $requestedName);
    }
}


