<?php

namespace Idmarinas\LoginFail2Ban\Factory;

use Idmarinas\LoginFail2Ban\LoginFail2Ban as LoginFail;
use Idmarinas\LoginFail2Ban\Options as Opciones;

use Interop\Container\ContainerInterface;

use Zend\Db\TableGateway\TableGateway;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class LoginFail2Ban implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $options = $container->get(Opciones::class);

        //-- Configurar la Base de datos
        $tableGateway = new TableGateway(
            $options->getTableName(),
            $container->get($options->getAdapterClassName())
        );

        $loginfail2ban = new LoginFail($tableGateway, $options);

        return $loginfail2ban;
    }

    public function createService(ServiceLocatorInterface $services, $canonicalName = null, $requestedName = null)
    {
        return $this($services, $requestedName);
    }
}