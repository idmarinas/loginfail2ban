<?php

namespace Idmarinas\LoginFail2Ban\Factory;

use Idmarinas\LoginFail2Ban\Listener\LoginFail2Ban;
use Idmarinas\LoginFail2Ban\Options;

use Interop\Container\ContainerInterface;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class LoginFail2BanListener implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $options = $container->get(Options::class);

		$listener = new LoginFail2Ban($container);

		$listener->setNamespace($options->getEventNamespaceName())
			->setEventError($options->getEventAuthenticationErrorName())
		;

		return $listener;
    }

    public function createService(ServiceLocatorInterface $services, $canonicalName = null, $requestedName = null)
    {
        return $this($services, $requestedName);
    }
}