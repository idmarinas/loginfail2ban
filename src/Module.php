<?php

/**
 * @copyright 2012-2016
 * @author    Iván Diaz Marinas
 *
 * @license   MIT
 * @link      https://bitbucket.org/idmarinas/loginfail2ban
 */

namespace Idmarinas\LoginFail2Ban;

use Zend\EventManager\EventInterface;

class Module
{

	public function onBootstrap(EventInterface $event)
	{
		$app = $event->getApplication();
		$em  = $app->getEventManager();
		$sm  = $app->getServiceManager();

		$LoginFail2BanListener = $sm->get(Listener\LoginFail2Ban::class);
		$LoginFail2BanListener->attach($em);
	}

	/**
	 * Retrieve default zend-session config for zend-mvc context.
	 *
	 * @return array
	 */
	public function getConfig()
	{
		$provider = new ConfigProvider();

		return [
			'service_manager' => $provider->getDependencyConfig(),
		];
	}
}
