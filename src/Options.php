<?php

namespace Idmarinas\LoginFail2Ban;

use Exception;

use Zend\Stdlib\AbstractOptions;
use Zend\EventManager\Event;

class Options extends AbstractOptions
{
	/**
	 * Tiempo que se va a bloquear la IP si se supera el número de veces permitidas
	 * @var int
	 */
	protected $blockDuration = 900; //15 minutos

	/**
	 * Número de intentos que se permiten antes de bloquear la IP
	 * @var int
	 */
	protected $maxCount = 5;

	/**
	 * Nombre del adaptador de la aplicación
	 * @var string
	 */
	protected $adapterClassName = 'Db\Adapter\Main';

	/**
	 * Nombre de la tabla donde se van a guardar los registros
	 * @var string
	 */
	protected $tableName = 'accounts_loginfail2ban';

	/**
	 * Nombre del evento que se activa ante un error en la autentificación
	 * @var string
	 */
	protected $eventAuthenticationErrorName = 'authentication.error';

	/**
	 * Nombre identificador para el evento
	 * @var string
	 */
	protected $eventNamespaceName = 'Account';

	/**
	 * Configurar el identificador para el evento
	 * @var string
	 *
	 * @return Idmarinas\LoginFail2Ban\Options
	 */
	public function setEventNamespaceName($name)
	{
		$this->eventNamespaceName = $name;

		return $this;
	}

	/**
	 * Obtener el identificador para el evento
	 * @return string
	 */
	public function getEventNamespaceName()
	{
		return $this->eventNamespaceName;
	}

	/**
	 * Configurar el nombre del evento que se activa ante un error en la autentificación
	 * @var string
	 *
	 * @return Idmarinas\LoginFail2Ban\Options
	 */
	public function setEventAuthenticationErrorName($name)
	{
		$this->eventAuthenticationErrorName = $name;

		return $this;
	}

	/**
	 * Obtener el nombre del evento que se activa ante un error en la autentificación
	 * @return string
	 */
	public function getEventAuthenticationErrorName()
	{
		return $this->eventAuthenticationErrorName;
	}

	/**
	 * Configurar el nombre de la tabla
	 * @var string
	 *
	 * @return Idmarinas\LoginFail2Ban\Options
	 */
	public function setTableName($name)
	{
		$this->tableName = $name;

		return $this;
	}

	/**
	 * Obtener el nombre de la tabla
	 * @return string
	 */
	public function getTableName()
	{
		return $this->tableName;
	}

	/**
	 * Configurar el evento que se activa ante un error en la autentificación
	 *
	 * @var string $name
	 *
	 * @return Idmarinas\LoginFail2Ban\Options
	 */
	public function setAuthenticationErrorClass($name)
	{
		$this->adapterClassName = $name;

		return $this;
	}

	/**
	 * Devolver el nombre del evento
	 */
	public function getAuthenticationErrorClass()
	{
		return $this->adapterClassName;
	}

	public function setNamespaceEvent($name)
	{
		$this->namespaceEvent = $name;

		return $this;
	}

	/**
	 * Devolver el nombre del evento
	 */
	public function getNamespaceEvent()
	{
		return $this->namespaceEvent;
	}

	/**
	 * Configurar el nombre del Adaptador
	 *
	 * @var string $name
	 *
	 * @return Idmarinas\LoginFail2Ban\Options
	 */
	public function setAdapterClassName($name)
	{
		$this->adapterClassName = $name;

		return $this;
	}

	/**
	 * Devolver el nombre del adaptador
	 */
	public function getAdapterClassName()
	{
		return $this->adapterClassName;
	}

	/**
	 * Configurar la duración del bloqueo de IP
	 *
	 * @param int $duration En segundos y mayor de 300
	 *
	 * @return $this
	 * @throws \Exception
	 *
	 */
	public function setBlockDuration($duration)
	{
		$duration = (int)$duration;
		if ($duration < 300)
		{
			throw new Exception("setBlockDuration '$duration' debe ser un número entero y mayor que 300 (en segundos)");
		}

		$this->blockDuration = $duration;

		return $this;
	}

	/**
	 * Obtener la duración del bloqueo
	 *
	 * @return int Tiempo en segundos que se bloquea al fallar una conexión
	 */
	public function getBlockDuration()
	{
		return $this->blockDuration;
	}

	/**
	 * Configurar el número máximo de fallos permitido
	 *
	 * @param int $count Número máximo de errores permitidos
	 *
	 * @return LoginFailBan
	 * @throws \Exception
	 */
	public function setMaxCount($count)
	{
		$count = (int)$count;

		if ($count > 0)
		{
			throw new \Exception("setMaxCount '$count' debe de ser un número entero mayor que 0.");
		}

		$this->maxCount = $count;

		return $this;
	}

	/**
	 * Obtener el número máximo de fallos permitido
	 *
	 * @return int
	 */
	public function getMaxCount()
	{
		return $this->maxCount;
	}
}