<?php

namespace Idmarinas\LoginFail2Ban\Listener;

use Idmarinas\LoginFail2Ban\LoginFail2Ban as LoginFail;
use Idmarinas\LoginFail2Ban\LoginFail2BanEvent;
use Idmarinas\LoginFail2Ban\Options;

use Interop\Container\ContainerInterface;

use Zend\Db\Sql\Predicate\Expression;

use Zend\EventManager\Event;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\ListenerAggregateTrait;

class LoginFail2Ban implements ListenerAggregateInterface
{
	use ListenerAggregateTrait;

	protected $container;
	protected $namespace;
	protected $eventError;

	public function __construct(ContainerInterface $container)
	{
		$this->container = $container;
	}

	public function attach(EventManagerInterface $events, $priority = 100)
    {
		$sharedEvents = $events->getSharedManager();

        $this->listeners[] = $sharedEvents->attach($this->namespace, LoginFail2BanEvent::EVENT_LOGIN2FAILBAN_DATA, [$this, 'onLogin2FailBanData'], $priority);
        $this->listeners[] = $sharedEvents->attach($this->namespace, $this->eventError, [$this, 'onAuthenticationError'], $priority);
    }

	/**
	 * Actualizar el número de intentos fallidos
	 */
	public function onAuthenticationError(Event $event)
	{
		$loginfail2ban = $this->container->get(LoginFail::class);
		$options = $this->container->get(Options::class);
		$blockDuration = $options->getBlockDuration();
		//-- Configurar los fallos cometidos
		$loginfail2ban->setCount($loginfail2ban->getCount()+1);

		$data = [];
		$data['count'] = $loginfail2ban->getCount();

		if ($loginfail2ban->getCount() >= $options->getMaxCount())
		{
			$duration = round($blockDuration + ($blockDuration * ($loginfail2ban->getBanCount()*1.1)));

			$loginfail2ban->setCount(0);
			$loginfail2ban->setBanCount($loginfail2ban->getBanCount()+1);
			$loginfail2ban->setBanEnd($duration + time());
			$loginfail2ban->setDate(strtotime(date('Y-m-d')));

			$data['count'] = 0;
			$data['banCount'] = $loginfail2ban->getBanCount();
			$data['banEnd'] = $loginfail2ban->getBanEnd();
			$data['date'] = $loginfail2ban->getDate();
		}

		$loginfail2ban->getTableGateway()->update(
			$data,
			[
				'ipAddress' => $loginfail2ban->getIpAddress(),
				'date >= ? ' => strtotime(date('Y-m-d'))
			]
		);

		$event->setParam('loginfail2ban', $loginfail2ban->getDataCache());
	}

	/**
	 * Pasar los datos de loginfail2ban
	 */
	public function onLogin2FailBanData(Event $event)
	{
		$loginfail2ban = $this->container->get(LoginFail::class);

		$event->setParam('loginfail2ban', $loginfail2ban->getDataCache());
	}

	public function setNamespace($name)
	{
		$this->namespace = $name;

		return $this;
	}

	public function setEventError($name)
	{
		$this->eventError = $name;

		return $this;
	}

	public function seEvenPre($name)
	{
		$this->evenPre = $name;

		return $this;
	}
}