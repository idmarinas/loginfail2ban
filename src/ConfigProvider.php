<?php

/**
 * @copyright 2012-2016
 * @author    Iván Diaz Marinas
 *
 * @license   MIT
 * @link      https://bitbucket.org/idmarinas/loginfail2ban
 */

namespace Idmarinas\LoginFail2Ban;

use Interop\Container\ContainerInterface;

class ConfigProvider
{
	/**
	 * Obtener la configuración para idmarinas-loginfail2ban
	 *
	 * @return array
	 */
	public function __invoke()
	{
		return [
			'dependencies' => $this->getDependencyConfig(),
		];
	}

	/**
	 * Retrieve dependency config for idmarinas-loginfail2ban
	 *
	 * @return array
	 */
	public function getDependencyConfig()
	{
		return [
			'factories' => [
				LoginFail2Ban::class => Factory\LoginFail2Ban::class,
				Options::class => Factory\Options::class,
				Listener\LoginFail2Ban::class => Factory\LoginFail2BanListener::class
			]
		];
	}
}
