<?php

/**
 * @copyright 2012-2016
 * @author    Iván Diaz Marinas
 *
 * @license   MIT
 * @link      https://bitbucket.org/idmarinas/loginfail2ban
 */

namespace Idmarinas\LoginFail2Ban;

use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\TableGateway;

use Zend\Session\Validator\HttpUserAgent;
use Zend\Session\Validator\RemoteAddr;

class LoginFail2Ban
{
	use Pattern\Data;

	/**
	 * Dirección IP que usa el usuario
	 * @var string
	 */
	protected $ipAddress;

	/**
	 * Navegador que está usando el usuario
	 * @var string
	 */
	protected $userAgent;

	/**
	 * Número de intentos erroneos de conexión
	 * @var int
	 */
	protected $failCount = 0;

	/**
	 * Número de bloqueos (Son las veces que se ha bloqueado una IP)
	 * Se borra cuando la IP no está bloqueada y al día siguiente
	 *
	 * @var int
	 */
	protected $banCount;

	/**
	 * Fecha en la que se agregó el registro
	 * @var int
	 */
	protected $date;

	/**
	 * Fecha en la que termina el bloqueo
	 * @var int
	 */
	protected $banEnd;

	/**
	* @var Idmarinas\LoginFail2Ban\Options
	*/
	protected $options;

	/**
	 * @var Zend\Db\TableGateway\TableGateway
	 */
	protected $tableGateway;

	/**
	 * Sobreescribir el constructor
	 *
	 * @var Idmarinas\LoginFail2Ban\Options
	 */
	public function __construct(TableGateway $table, Options $options)
	{
		$this->tableGateway = $table;
		$this->options = $options;

		$http = new HttpUserAgent();
        $ipAddress = new RemoteAddr();

        $this->setIpAddress($ipAddress->getData());
        $this->setUserAgent($http->getData());

        $this->updateData();

		$this->deleteOldData();
	}

	/**
	 * Obtener la instancia TableGateway
	 * @return Zend\Db\TableGateway
	 */
	public function getTableGateway()
	{
		return $this->tableGateway;
	}

	/**
	 * Comprobar si la IP está bloqueada
	 *
	 * @return boolean
	 */
	public function isBlocked()
	{
		if ($this->getBanEnd() >= time()) return true;
		else if ($this->getCount() >= $this->options->getMaxCount()) return true;

		return false;
	}

	/**
	 * Obtener el número máximo de intentos
	 *
	 * @return int
	 */
	public function getMaxCount()
	{
		return $this->options->getMaxCount();
	}
	/**
	 * Configurar la dirección IP
	 * @var string $ip
	 */
	public function setIpAddress($ip)
	{
		$this->ipAddress = $ip;

		return $this;
	}

	/**
	 * Obtener la dirección IP
	 * @return string
	 */
	public function getIpAddress()
	{
		return $this->ipAddress;
	}

	/**
	 * Configurar el navegador que usa el usuario
	 * @var string $agent
	 */
	public function setUserAgent($agent)
	{
		$this->userAgent = $agent;

		return $this;
	}

	/**
	 * Obtener el navegador que usa el usuario
	 * @return string
	 */
	public function getUserAgent()
	{
		return $this->userAgent;
	}

	/**
	 * Configurar el número de intentos fallidos actuales
	 */
	public function setCount($count)
	{
		$this->failCount = $count;

		return $this;
	}

	/**
	 * Obtener el número de intentos fallidos actuales
	 *
	 * @return int
	 */
	public function getCount()
	{
		return $this->failCount;
	}

	/**
	 * Obtener el número de bloqueos (Son las veces que se ha bloqueado una IP)
	 *
	 * @var int $count
	 */
	public function setBanCount($count)
	{
		$this->banCount = $count;

		return $this;
	}

	/**
	 * Obtener el número de bloqueos (Son las veces que se ha bloqueado una IP)
	 *
	 * @return int
	 */
	public function getBanCount()
	{
		return $this->banCount;
	}

	/**
	 * Obtener la fecha en la que finaliza el bloqueo
	 * @var int $time
	 */
	public function setBanEnd($time)
	{
		$this->banEnd = $time;

		return $this;
	}

	/**
	 * Obtener la fecha en la que finaliza el bloqueo
	 * @return int
	 */
	public function getBanEnd()
	{
		return $this->banEnd;
	}

	/**
	 * Configurar la fecha en la que se agrego el registro
	 * @var int
	 *
	 * @return Idmarinas\LoginFail2Ban\LoginFail2Ban
	 */
	public function setDate($date)
	{
		$this->date = $date;

		return $this;
	}

	/**
	 * Obtener la fecha en la que se agregó el registro
	 * @return int
	 */
	public function getDate()
	{
		return $this->date;
	}
}