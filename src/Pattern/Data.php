<?php

namespace Idmarinas\LoginFail2Ban\Pattern;

use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;

use Zend\Math\Rand;

trait Data
{
	/**
	 * Obtener información sobre los intentos de conexión de la IP
	 *
	 * @return array
	 */
	public function updateData()
	{
		$row = $this->getTableGateway()->select(function(Select $select){
			$select->limit(1)
				->where->equalTo('ipAddress', $this->getIpAddress())
					->greaterThanOrEqualTo('date', strtotime(date('Y-m-d')))
			;
		})->current();

		if (!$row)
		{
			$row = [
				'ipAddress' => $this->getIpAddress(),
				'count' => 0,
				'date' => strtotime(date('Y-m-d')),
				'banEnd' => 0,
				'banCount' => 0,
				'userAgent' => $this->getUserAgent()
			];

			$this->getTableGateway()->insert($row);
		}

        $this->setCount($row['count'])
        	->setDate($row['date'])
        	->setBanEnd($row['banEnd'])
        	->setBanCount($row['banCount'])
		;
	}

	/**
	 * Recuperar los datos sin hacer otra consulta a la base de datos
	 */
	public function getDataCache()
	{
		return [
			'ipAddress' => $this->getIpAddress(),
			'count' => $this->getCount(),
			'maxCount' => $this->getMaxCount(),
			'date' => $this->getDate(),
			'banEnd' => $this->getBanEnd(),
			'banCount' => $this->getBanCount(),
			'userAgent' => $this->getUserAgent(),
			'isBlocked' => $this->isBlocked()
		];
	}

	/**
	 * Borrar entradas antiguas
	 */
	private function deleteOldData()
	{
		$integer = Rand::getInteger(0, 100);

		if (20 < $integer) return;

		$where = new Where();
		$where->lessThan('date', strtotime(date('Y-m-d')));

		$this->getTableGateway()->delete($where);

		return;
	}
}