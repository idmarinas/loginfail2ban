<?php

namespace Idmarinas\LoginFail2Ban;

use Zend\EventManager\Event;

class LoginFail2BanEvent extends Event
{
	const EVENT_LOGIN2FAILBAN_DATA = 'login2failban.data';
}